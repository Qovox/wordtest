# JavaFX

JavaFX est une bibliothèque (le remplaçant de swing) en Java qui permet de réaliser des applications avec GUI.

## Principe de fonctionnement

Pour faire d'une application Java une application JavaFX, il faut étendre notre classe principale de `Application` :
```java
public class Main extends Application {
	...
}
```

Ensuite, cela nous oblige à implémenter la méthode abstraite `start` venant de la classe abstraite `Application`. Cette méthode sera le point d'entrée d'une application JavaFX :
```java
@Override
public void start(Stage primaryStage) throws Exception {
	...
}
```

Pour garder une certaine compatibilité et permettre l'inclusion de code swing ou même le packaging de notre application en `.jar`, il faut rajouter la méthode `wordtest` constituant le point d'entrée de toute application standard, de cette façon :
```java
public static void wordtest(String[] args) {
	launch(args);
}
```

La méthode `launch` est en fait la méthode qui est automatiquement appelée quand on ne met de méthode wordtest dans notre programme. C'est en fait une méthode statique venant de la classe abstraite `Application`.

L'interface utilisateur est contenue dans un `Stage` JavaFX, qui est container de plus haut niveau dans JavaFX. Le _primary Stage_ est construit automatiquement par la plateforme et passé en argument au point d'entrée de l'application.

Une `Scene` contient tous les éléments d'interface. Elle a un _root node_, ainsi qu'une largeur et hauteur. Un _root node_ peut en fait être soit un simple boutton, ou encore label, ou bien ça peut être un groupe de _nodes_ tel qu'un `GridPane`.

## JavaFX Lifecycle

Quand une appli JavaFX est lancée, voici ce qui se passe :
+ La méthode `init()` est appelée avant que quoi que ce soit ne soit réalisé. De base, elle est implémentée dans la classe `Application` mais ne fait rien. Si l'utilisateur veut réaliser de quelconques actions au démarrage de son application, il lui suffit d'`Override` cette méthode
+ Ensuite, la méthode `start()` (que nous connaissons) est appelée
+ L'application est maintenant en train de tourner, et le Runtime n'attend maintenant qu'une chose : la fermeture de l'application, qui peut survenir soit :
	1. Quand l'application fait appel à `Platform.exit()`
	2. La dernière fenêtre de l'application est fermée
+ Enfin, avant que l'application ne se ferme, la méthode `stop()` de la classe `Application` est appelée, on peut donc comme pour la méthode `init()` l'`Override` et faire ce que l'on souhaite dedans (généralement du nettoyage, destruction de ressources utilisées par l'application ou encore sauvergarde de données).

## Properties

En JavaFX, lorsque l'on crée des classes de model, il est courant et pratique d'utiliser des `Property` plutôt que des types primitifs classiques pour caractériser les attributs d'un objet. Les properties permettent en fait d'introduire un data binding, ouvrant alors la possibilité d'ajouter facilement des _listeners_ par exemple.

## JFoenix Demo Code Analyze

### FlowContext

```java
@FXMLViewFlowContext
private ViewFlowContext flowContext;
```
Un `ViewFlowContext` une fois injecté à l'aide de l'annotation `FXMLViewFlowContext` permet d'enregistrer des données de l'application qui pourront ensuite être récupérées et réutilisées à travers toute l'application et notamment dans les controllers.

On peut alors register des données de cette manière :
```java
flowcontext = new ViewFlowContext();
flowcontext.register("Stage", stage);
```

### Flow

Un flow est une map de plusieurs vues (_views_) liées entre elles. Un flow permet donc d'effectuer des opérations sur ces vues, de manière indépendante sur chaque vue, ou bien encore une action groupée sur chaque vue du flow.

Le constructeur d'un objet `Flow` prend en paramètre un controller :
```java
Flow flow = new Flow(MainController.class);
```

Un flow a besoin d'un noeud JavaFX parent dans lequel la vue actuelle se verra ajouter la vue du flow comme enfant.
Donc la classe `DefaultFlowContainer` va fournir une implémentation par défaut de l'interface `FlowContainer`, en encapsulant le flow dans un container parent étant un StackPane.

Ensuite, pour pouvoir run le flow, il faut créer un `FlowHandler` qui pourra s'en occuper :
```java
DefaultFlowContainer container = new DefaultFlowContainer();
flow.createHandler(flowContext).start(container);
```
On appelle ensuite la méthode `start()` sur le `FlowHandler` pour démarrer le flow.

### JFXDecorator

C'est ce qui permet d'ajouter les fonctions de fermeture, maximisation, minimisation et fullscreen de la fenêtre.
Pour construire un tel objet, le constructeur a besoin du primary stage utilisé par l'application, ainsi que du noeud à décoré.

Ainsi :
```java
JFXDecorator decorator = new JFXDecorator(stage, container.getView());
```

---

Dans la suite du code, on peut prendre les mesures de l'écran grâce à :
```java
Rectangle2D bounds = Screen.getScreens().get(0).getBounds();
width = bounds.getWidth() / 2.5;
height = bounds.getHeight() / 1.35;
```

Ils initialisent la scène avec :
```java
Scene scene = new Scene(decorator, width, height);
```

Ensuite, très classiquement on set la scène du stage et on montre le stage :
```java
stage.setScene(scene);
stage.show();
```

### MainController & Controller

De ce que j'ai compris c'est le MainController passé dans le flow qui permet d'afficher tout ce qui se trouve à l'interieur de la scène, et du décorateur.

Vu qu'on utilise _DataFX_ un controlleur doit être annoté de `@ViewController(value = "/path/from/resources/directory/to/fxml/source")` à minima. Ensuite on peut rajouter un titre pour qui sera utilisé quand par exemple la vue sera montrée dans un control `Tab`.

Quand on a plusieurs éléments qui constituent une vue, pour pouvoir y accéder et les contrôler dans le controlleur associé à la vue, il faut injecter chaque composant que l'on souhaite controler à l'aide de l'annotation `@FXML`. Une chose très importante à retenir, pour que l'injection marche, il faut que le nom de l'attribut soit le même que le `fx:id` défini dans le `.fxml`.

Ce qu'il faut retenir, c'est que c'est dans le controller que tout le code logique se trouvera. Ce qui en soi consitue d'ailleurs un problème car on pourra très facilement se retrouver avec des controlleurs ressemblant à des god class pour peu que notre application soit mal découpée. On pourra et retrouvera aussi certainement du code logique qui sert à modifier la vue. Ce qui, dans certains cas peut paraître pas très beau et pas très propre.

### FXML Loader



### CSS

J'ai rencontré un problème au niveau des `JFXTextArea`. Celles-ci ne semblaient pas vouloir se resize au-delà d'un certain nombre de pixel. La solution se trouvait au niveau du CSS où les `-jfx-text-area` étaient cappées à une `-fx-max-width: 300.0;`.

Il faut donc toujours penser à checker si le CSS n'est pas en cause lorsqu'un control n'a PAS le comportement attendu que l'on souhaite lui donnner dans le code. Il semblerait en effet que le css ait la priorité par rapport au code.

### JFXRippler

Ce sont simplement les effets super cool qui apparaissent quand on clique sur n'importe quel control. Les ripplers peuvent être appliqués à n'importe quel node.

### JFXHamburger

Ce sont les 3 petits traits superposés aux bouts arrondis sur lesquels si on clique, un drawer s'affiche.

## Java Snippet Sample

```java

```

## Java Language Specifications

Rappel :
+ En java le mot clé **`final`** peut être utilisé pour :
	1. **Des classes** : De telles classe ne peuvent tout simplement pas être héritée.
	2. **Des méthodes** : De telles méthodes ne peuvent pas être cachées ou overridden par des classes filles.
	3. **Des variables** : De telles variables ne peuvent être initialisées qu'une seule et unique fois. Ceci dit, aucune obligation de le faire au moment de la déclaration.

# Application code snippet

Old code to set gridpane row and column constraints.

```java
RowConstraints rc = new RowConstraints();
rc.setPercentHeight(100);
rc.setValignment(VPos.CENTER);
subRoot.getRowConstraints().add(rc);

ColumnConstraints cc = new ColumnConstraints();
cc.setPercentWidth(20);
subRoot.getColumnConstraints().add(cc);
cc = new ColumnConstraints();
cc.setPercentWidth(30);
subRoot.getColumnConstraints().add(cc);
cc = new ColumnConstraints();
cc.setPercentWidth(30);
subRoot.getColumnConstraints().add(cc);
cc = new ColumnConstraints();
cc.setPercentWidth(20);
subRoot.getColumnConstraints().add(cc); 
```