package app;

import app.constants.AppSettings;
import app.controllers.MainController;
import app.models.Word;
import app.models.WordList;
import app.utils.Utils;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonPrimitive;
import com.jfoenix.controls.JFXDecorator;
import com.jfoenix.svg.SVGGlyph;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.container.DefaultFlowContainer;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static app.constants.AppSettings.*;

public class Main extends Application {

    @FXMLViewFlowContext
    private ViewFlowContext context;

    public static List<WordList> wordList = new ArrayList<>();

    @Override
    public void init() {
        readWordLists();
    }

    @Override
    public void start(Stage stage) throws Exception {
        SetupEnvironment();

        Flow flow = new Flow(MainController.class);
        DefaultFlowContainer container = new DefaultFlowContainer();
        context = new ViewFlowContext();
        context.register("Stage", stage);
        Utils.setStage(stage);
        flow.createHandler(context).start(container);

        JFXDecorator decorator = new JFXDecorator(stage, container.getView());

        decorator.setCustomMaximize(true);
        decorator.setGraphic(new SVGGlyph(""));

        stage.setTitle("Word Test");
        stage.setMinWidth(MIN_WIDTH);
        stage.setMinHeight(MIN_HEIGHT);
        stage.setMaxWidth(MAX_WIDTH - WIDTH_MARGIN);
        stage.setMaxHeight(MAX_HEIGHT - HEIGHT_MARGIN);

        Scene scene = new Scene(decorator, MIN_WIDTH, MIN_HEIGHT);

        final ObservableList<String> stylesheets = scene.getStylesheets();
        stylesheets.addAll(Main.class.getResource("/css/app.css").toExternalForm(),
                Main.class.getResource("/css/app_design.css").toExternalForm(),
                Main.class.getResource("/css/app_fonts.css").toExternalForm());

        stage.setScene(scene);
        stage.show();
        stage.centerOnScreen();
    }

    private boolean SetupEnvironment() {
        File resource = new File(DIR_RESOURCES);
        if (!resource.exists() && resource.mkdir()) {
            File wordLists = new File(resource.getPath(), DIR_WORD_LISTS);

            if (!wordLists.exists() && wordLists.mkdir()) {
                return true;
            }
        }

        return false;
    }

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void stop() {

    }

    private void readWordLists() {
        File folder = new File(AppSettings.DIR_RESOURCES + AppSettings.DIR_WORD_LISTS);
        File[] listOfFiles = folder.listFiles();

        for (int i = 0; i < (listOfFiles != null ? listOfFiles.length : 0); i++) {
            if (listOfFiles[i].isFile()) {
                JsonParser parser = new JsonParser();
                JsonObject jsonWordList = parser.parse(Utils.readFile(new File(AppSettings.DIR_RESOURCES + AppSettings.DIR_WORD_LISTS, listOfFiles[i].getName()))).getAsJsonObject();
                String ol = jsonWordList.get("original_language").getAsString();
                String tl = jsonWordList.get("translated_language").getAsString();
                JsonObject list = jsonWordList.getAsJsonObject("list");
                List<Word> original = new ArrayList<>();
                List<Word> translated = new ArrayList<>();

                for (Map.Entry s : list.entrySet()) {
                    Word ow = new Word(s.getKey().toString(), ol);
                    Word tw = new Word(((JsonPrimitive) s.getValue()).getAsString(), ow, tl);
                    ow.setTranslation(tw);
                    original.add(ow);
                    translated.add(tw);
                }

                WordList w = new WordList(jsonWordList.get("name").getAsString(), original, translated, ol, tl);
                wordList.add(w);
            }
        }
    }
}
