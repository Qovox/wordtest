package app.constants;

import app.controllers.AddEditWordListsController;
import app.controllers.PlayController;
import javafx.geometry.Rectangle2D;
import javafx.stage.Screen;

import static java.lang.StrictMath.round;

/**
 * The application settings used everywhere in the code to access useful information.
 * Performs initialization of the minimum and maximum width and height the window can have
 * according to native screen resolution.
 *
 * @author Kovox
 * @version 1.0
 */
public final class AppSettings {

    // Files
    public static final String DIR_RESOURCES = "resources/";
    public static final String DIR_WORD_LISTS = "WordLists/";

    public static final Class HOME_CONTENT = AddEditWordListsController.class;
    public static String currentController = "";

    public static final double MIN_WIDTH;
    public static final double MIN_HEIGHT;

    public static final double MAX_WIDTH;
    public static final double MAX_HEIGHT;

    private static final double RESIZE_MARGIN = 0.1;
    public static final double WIDTH_MARGIN;
    public static final double HEIGHT_MARGIN;

    // Controller constants
    public static final String PLAY_CONTROLLER = PlayController.class.getSimpleName();
    public static final String ADD_WORD_LISTS_CONTROLLER = AddEditWordListsController.class.getSimpleName();

    static {
        Rectangle2D bounds = Screen.getScreens().get(0).getBounds();

        MAX_WIDTH = bounds.getWidth();
        MAX_HEIGHT = bounds.getHeight();

        WIDTH_MARGIN = round(MAX_WIDTH * RESIZE_MARGIN);
        HEIGHT_MARGIN = round(MAX_HEIGHT * RESIZE_MARGIN);

        MIN_WIDTH = MAX_WIDTH / 2;
        MIN_HEIGHT = MAX_HEIGHT / 2;
    }

    private AppSettings() {

    }
}
