package app.controllers;

import app.Main;
import app.models.Word;
import app.models.WordList;
import app.utils.Utils;
import com.jfoenix.controls.*;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.beans.value.ObservableValue;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@ViewController(value = "/fxml/play.fxml")
public class PlayController {

    @FXMLViewFlowContext
    private ViewFlowContext context;

    @FXML
    private GridPane root;

    @FXML
    private VBox configuration;
    @FXML
    private JFXComboBox<WordList> wordListPicker;


    @FXML
    private GridPane preview;
    @FXML
    private JFXTextArea originalPreview;
    @FXML
    private JFXTextArea translatedPreview;

    @FXML
    private VBox toggles;
    @FXML
    private JFXToggleButton randomOrder;
    @FXML
    private JFXToggleButton translatedToOriginal;
    @FXML
    private JFXToggleButton withTimer;
    @FXML
    private VBox timer;
    @FXML
    private JFXButton playButton;

    @FXML
    private GridPane playArea;
    @FXML
    private GridPane headerArea;

    @FXML
    private Label overallCounter;
    @FXML
    private Label correctCounter;
    @FXML
    private Label mistakeCounter;

    @FXML
    private GridPane wordArea;
    @FXML
    private Label wordField;
    @FXML
    private HBox guessHBox;
    @FXML
    private JFXTextField guessField;
    @FXML
    private JFXListView<Label> answers;

    @FXML
    private HBox toolbar;
    @FXML
    private JFXButton hint;
    @FXML
    private JFXButton giveUp;

    @FXML
    private GridPane finishArea;
    @FXML
    private JFXButton yes;
    @FXML
    private JFXButton no;

    private JFXComboBox<String> timerPerWord = new JFXComboBox<>();
    private JFXComboBox<String> timerGameSec = new JFXComboBox<>();
    private JFXComboBox<String> timerGameMin = new JFXComboBox<>();

    private static String[] time = new String[61];
    private boolean randomize = false;
    private boolean reverseLanguage = false;

    // Run Attributes
    private List<Word> referenceList = new ArrayList<>();
    private int wordCounter = 1;
    private int correctAnswerCounter = 0;
    private int wrongAnswerCounter = 0;
    private WordList currentWordList = null;
    private Word currentWord = null;
    private boolean hintGiven = false;
    private int hintLevel = 0;

    // Constants
    private static final String BUTTON_STYLE = "custom-jfx-button-raised";
    private static final String OVERALL_COUNTER = "Word counter: ";
    private static final String CORRECT_COUNTER = "Correct answer(s): ";
    private static final String MISTAKE_COUNTER = "Wrong answer(s): ";

    static {
        for (int i = 0; i < time.length; ++i) {
            time[i] = String.valueOf(i);
        }
    }

    @PostConstruct
    public void init() {
        wordListPicker.prefWidthProperty().bind(configuration.widthProperty());
        timerPerWord.prefWidthProperty().bind(configuration.widthProperty());
        timerGameSec.prefWidthProperty().bind(configuration.widthProperty());
        timerGameMin.prefWidthProperty().bind(configuration.widthProperty());
        playButton.prefWidthProperty().bind(configuration.widthProperty());
        originalPreview.prefWidthProperty().bind(preview.widthProperty());
        originalPreview.prefHeightProperty().bind(preview.heightProperty());
        translatedPreview.prefWidthProperty().bind(preview.widthProperty());
        translatedPreview.prefHeightProperty().bind(preview.heightProperty());
        wordField.prefHeightProperty().bind(wordArea.heightProperty());
        guessHBox.prefWidthProperty().bind(wordArea.widthProperty());
        guessField.prefWidthProperty().bind(guessHBox.widthProperty());
        answers.prefWidthProperty().bind(preview.widthProperty());
        answers.prefHeightProperty().bind(preview.heightProperty());

        originalPreview.setBorder(new Border(new BorderStroke(new Color(0.1961, 0.1961, 0.1961, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));
        translatedPreview.setBorder(new Border(new BorderStroke(new Color(0.1961, 0.1961, 0.1961, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));

        update();

        wordListPicker.setCellFactory(cell -> Utils.wordListCell());
        wordListPicker.onMouseClickedProperty().set(e -> {
            if (wordListPicker.getItems().size() != Main.wordList.size()) {
                update();
            }
        });
        wordListPicker.getSelectionModel().selectedIndexProperty().addListener((observable, oldValue, newValue) -> {
            currentWordList = wordListPicker.getSelectionModel().getSelectedItem();

            if (currentWordList == null) {
                originalPreview.setPromptText("Original Language: ");
                translatedPreview.setPromptText("Translated Language: ");
            } else {
                originalPreview.setPromptText("Original Language: " + currentWordList.getOriginalLanguage());
                originalPreview.setText(currentWordList.getOriginalToString());
                translatedPreview.setPromptText("Translated Language: " + currentWordList.getTranslatedLanguage());
                translatedPreview.setText(currentWordList.getTranslatedToString());
            }

            playButton.setDisable(false);
        });

        randomOrder.selectedProperty().addListener((observable, oldValue, newValue) -> randomize = newValue);
        translatedToOriginal.selectedProperty().addListener((observable, oldValue, newValue) -> reverseLanguage = newValue);

        withTimer.selectedProperty().addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                timer.getChildren().add(timerPerWord);
                timer.getChildren().add(timerGameSec);
                timer.getChildren().add(timerGameMin);
            } else {
                timer.getChildren().clear();
            }
        });

        timerPerWord.setPromptText("Timer per word (in seconds)");
        timerGameSec.setPromptText("Game timer (in seconds)");
        timerGameMin.setPromptText("Game timer (in minutes)");

        timerPerWord.setLabelFloat(true);
        timerGameSec.setLabelFloat(true);
        timerGameMin.setLabelFloat(true);

        timerPerWord.getItems().setAll(time);
        timerGameSec.getItems().setAll(time);
        timerGameMin.getItems().setAll(time);

        timerPerWord.getItems().remove(0);
        timerGameSec.getItems().remove(60);
        timerGameSec.getItems().remove(0);
        timerGameMin.getItems().remove(0);

        playButton.setButtonType(JFXButton.ButtonType.RAISED);
        playButton.getStyleClass().add(BUTTON_STYLE);
        playButton.setDisable(true);

        hint.setButtonType(JFXButton.ButtonType.RAISED);
        hint.getStyleClass().add(BUTTON_STYLE);

        giveUp.setButtonType(JFXButton.ButtonType.RAISED);
        giveUp.getStyleClass().add(BUTTON_STYLE);

        guessField.textProperty().addListener(this::guessFieldTextPropertyListener);
        guessField.setOnKeyPressed(e -> {
            if (e.getCode() == javafx.scene.input.KeyCode.ENTER) {
                hintLevel = 0;
                hintGiven = false;

                Label answer = new Label(wordField.getText() + " -> " + guessField.getText());

                if (guessField.getText().equals(currentWord.getTranslation().getWord())) {
                    correctAnswerCounter++;
                    updateHeaderLabels();
                    answer.setTextFill(Color.valueOf("#0F9D58"));
                } else {
                    wrongAnswerCounter++;
                    updateHeaderLabels();
                    answer.setTextFill(Color.valueOf("#f44336"));
                }

                answers.getItems().add(answer);
                answers.scrollTo(answer);

                if (wordCounter >= currentWordList.getOriginal().size()) {
                    wordArea.setVisible(false);
                    finishArea.setVisible(true);
                    giveUp.setDisable(true);
                } else {
                    overallCounter.setText(OVERALL_COUNTER + ++wordCounter + "/" + referenceList.size());
                    currentWord = referenceList.get(wordCounter - 1);
                    wordField.setText(currentWord.getWord());
                    guessField.setText(null);
                }
            }
        });

        yes.setButtonType(JFXButton.ButtonType.RAISED);
        yes.getStyleClass().add(BUTTON_STYLE);

        no.setButtonType(JFXButton.ButtonType.RAISED);
        no.getStyleClass().add(BUTTON_STYLE);

        // Debug purpose
        //wordField.setBorder(new Border(new BorderStroke(Color.GREEN, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(3))));
        //wordLists.setBorder(new Border(new BorderStroke(Color.GREEN, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(3))));
    }

    private void update() {
        Main.wordList.sort(Comparator.comparing(WordList::getName));
        wordListPicker.getItems().setAll(Main.wordList);
    }

    @FXML
    private void play() {
        Utils.DIALOG_CONTROLLER.configBinaryChoice(Modality.APPLICATION_MODAL,
                "Get Ready",
                "Type the correct word translation and hit enter to validate. You can get some hint at every " +
                        "moment if needed. You can also give up at any moment if needed.\n\n Get started?",
                (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));

        if (Utils.DIALOG_CONTROLLER.getResult() == DialogController.DialogResult.YES) {
            configuration.setDisable(true);
            preview.setVisible(false);
            playArea.setVisible(true);

            if (reverseLanguage) {
                referenceList.addAll(currentWordList.getTranslated());

                if (randomize) {
                    Collections.shuffle(referenceList);
                } else {
                    referenceList.sort(Comparator.comparing(Word::getWord));
                }
            } else {
                referenceList.addAll(currentWordList.getOriginal());

                if (randomize) {
                    Collections.shuffle(referenceList);
                } else {
                    referenceList.sort(Comparator.comparing(Word::getWord));
                }
            }

            currentWord = referenceList.get(0);
            overallCounter.setText(OVERALL_COUNTER + wordCounter + "/" + referenceList.size());
            wordField.setText(currentWord.getWord());
        }
    }

    private void updateHeaderLabels() {
        correctCounter.setText(CORRECT_COUNTER + correctAnswerCounter + "/" + wordCounter);
        mistakeCounter.setText(MISTAKE_COUNTER + wrongAnswerCounter + "/" + wordCounter);
    }

    private void guessFieldTextPropertyListener(ObservableValue<? extends String> observable, String oldValue, String newValue) {
        // Hint button treatment
        if (hintGiven) {
            if (newValue.length() - hintLevel == 0) {
                hint.setDisable(false);
            } else if (newValue.length() - hintLevel < 0) {
                guessField.setText(oldValue);
            } else {
                hint.setDisable(true);
            }
        } else {
            if (newValue == null || newValue.isEmpty()) {
                hint.setDisable(false);
            } else {
                hint.setDisable(true);
            }
        }
    }

    @FXML
    private void hint() {
        hintGiven = true;

        if (hintLevel < currentWord.getTranslation().getWord().length()) {
            guessField.appendText(String.valueOf(currentWord.getTranslation().getWord().toCharArray()[hintLevel++]));
        } else {
            Utils.DIALOG_CONTROLLER.configAccept(JFXDialog.DialogTransition.CENTER,
                    "Well...",
                    "The word is completed now... just hit enter!",
                    (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));
        }
    }

    @FXML
    private void giveUp() {
        resetConfiguration();
        configuration.setDisable(false);
        playArea.setVisible(false);
        finishArea.setVisible(false);
        wordArea.setVisible(true);
        preview.setVisible(true);
        resetPlayArea();
    }

    private void resetConfiguration() {
        wordListPicker.getSelectionModel().clearSelection();
        randomOrder.selectedProperty().setValue(false);
        translatedToOriginal.selectedProperty().setValue(false);
        withTimer.selectedProperty().setValue(false);
        timerPerWord.getSelectionModel().clearSelection();
        timerGameSec.getSelectionModel().clearSelection();
        timerGameMin.getSelectionModel().clearSelection();
        originalPreview.setText(null);
        translatedPreview.setText(null);
        playButton.setDisable(true);
    }

    private void resetPlayArea() {
        referenceList.clear();
        answers.getItems().clear();
        currentWord = null;
        wordCounter = 1;
        correctAnswerCounter = 0;
        wrongAnswerCounter = 0;
        overallCounter.setText(OVERALL_COUNTER + "0/0");
        correctCounter.setText(CORRECT_COUNTER + "0/0");
        mistakeCounter.setText(MISTAKE_COUNTER + "0/0");
        hintGiven = false;
        hintLevel = 0;
        guessField.setText(null);
        wordField.setText("WordToGuess");
        giveUp.setDisable(false);
    }

    @FXML
    private void retry() {
        resetPlayArea();
        wordArea.setVisible(true);
        finishArea.setVisible(false);
        play();
    }
}
