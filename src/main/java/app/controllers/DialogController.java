package app.controllers;

import app.utils.Utils;
import com.jfoenix.controls.JFXAlert;
import com.jfoenix.controls.JFXButton;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXDialogLayout;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import javax.annotation.PostConstruct;

/**
 * Dialog controller allowing to get control over popup dialogs and their content.
 *
 * @author Kovox
 * @version 1.0
 */
@ViewController(value = "/fxml/popup/dialog.fxml")
public class DialogController {

    @FXMLViewFlowContext
    private ViewFlowContext context;

    @FXML
    private JFXDialog dialog;

    @FXML
    private JFXDialogLayout dialogLayout;

    @FXML
    private Label heading;

    @FXML
    private Label body;

    @FXML
    private JFXButton acceptButton;

    @FXML
    private JFXButton denyButton;

    private JFXAlert<DialogResult> alert;

    public enum DialogMode {ACCEPT, BINARY_CHOICE, TERNARY_CHOICE}
    public enum DialogResult {YES, NO, CANCEL, ACCEPT}

    private DialogMode mode = DialogMode.ACCEPT;
    private DialogResult result = DialogResult.ACCEPT;

    @PostConstruct
    private void init() {

    }

    /**
     * Accepts closing dialog.
     */
    @FXML
    public void acceptCloseDialog() {
        switch (mode) {
            case ACCEPT:
                result = DialogResult.ACCEPT;
                break;
            case BINARY_CHOICE:
            case TERNARY_CHOICE:
                result = DialogResult.YES;
                break;
        }

        dialog.close();
    }

    /**
     * Denies closing dialog.
     */
    @FXML
    public void denyCloseDialog() {
        switch (mode) {
            case BINARY_CHOICE:
            case TERNARY_CHOICE:
                result = DialogResult.NO;
                break;
        }

        dialog.close();
    }

    public JFXDialog getDialog() {
        return dialog;
    }

    public JFXDialogLayout getDialogLayout() {
        return dialogLayout;
    }

    public void setHeading(String text) {
        heading.setText(text);
    }

    public void setBody(String text) {
        body.setText(text);
    }

    public void config(JFXDialog.DialogTransition transition, String heading, String body, StackPane stackPane) {
        dialog.setTransitionType(transition);
        setHeading(heading);
        setBody(body);
        dialog.show(stackPane);
    }

    public void configAccept(JFXDialog.DialogTransition transition, String heading, String body, StackPane stackPane) {
        mode = DialogMode.ACCEPT;
        acceptButton.setText("Got it!");
        dialogLayout.setActions(acceptButton);
        dialog.setContent(dialogLayout);
        config(transition, heading, body, stackPane);
    }

    public void configBinaryChoice(Modality modality, String heading, String body, StackPane stackPane) {
        // Ensure that the user can't close the alert.
        alert = new JFXAlert<>((Stage) Utils.stage.getScene().getWindow());
        alert.initModality(modality);
        alert.setOverlayClose(false);

        setHeading(heading);
        setBody(body);

        // Buttons get added into the actions section of the layout.
        JFXButton addButton = new JFXButton("Yes");
        addButton.setDefaultButton(true);
        addButton.setOnAction(addEvent -> {
            // When the button is clicked, we set the result accordingly
            alert.setResult(DialogResult.YES);
            alert.hideWithAnimation();
        });

        JFXButton cancelButton = new JFXButton("No");
        cancelButton.setCancelButton(true);
        cancelButton.setOnAction(closeEvent -> {
            alert.setResult(DialogResult.NO);
            alert.hideWithAnimation();
        });

        mode = DialogMode.BINARY_CHOICE;
        dialogLayout.setActions(addButton, cancelButton);
        alert.setContent(dialogLayout);
        alert.showAndWait().ifPresent(dialogResult -> result = dialogResult);
    }

    public DialogMode getMode() {
        return mode;
    }

    public DialogResult getResult() {
        return result;
    }
}
