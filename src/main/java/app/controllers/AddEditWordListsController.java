package app.controllers;

import app.Main;
import app.constants.AppSettings;
import app.models.Word;
import app.models.WordList;
import app.utils.Utils;
import com.google.gson.JsonObject;
import com.jfoenix.controls.*;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.input.DragEvent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.stage.Modality;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.util.*;
import java.util.stream.IntStream;

@ViewController(value = "/fxml/add_edit_word_lists.fxml")
public class AddEditWordListsController {

    @FXMLViewFlowContext
    private ViewFlowContext context;

    @FXML
    private GridPane root;

    @FXML
    private VBox wordListsContainer;

    @FXML
    private JFXListView<WordList> wordLists;
    @FXML
    private JFXTextArea original;
    @FXML
    private JFXTextArea translated;
    @FXML
    private JFXTextArea originalPreview;
    @FXML
    private JFXTextArea translatedPreview;

    @FXML
    private VBox manager;
    @FXML
    private JFXTextField listName;
    @FXML
    private JFXTextField originalLanguage;
    @FXML
    private JFXTextField translatedLanguage;
    @FXML
    private JFXTextField originalFile;
    @FXML
    private JFXTextField translatedFile;

    @FXML
    private JFXButton addListBtn;
    private JFXButton delListBtn;

    private static final String EMPTY_LINE = "\\n[\\s]*\\n";
    private final ObservableList<WordList> wordListsDynamic = FXCollections.observableList(
            new ArrayList<>(), (WordList wl) -> new ObservableValue[]{wl.nameProperty()});
    private int previousSelectedIndex = -1;
    private WordList selectedWordList;
    private boolean editMode = false;

    @PostConstruct
    public void init() {
        Map<String, String> extensionFilters = new HashMap<>();
        extensionFilters.put("All text files", "*.*");
        extensionFilters.put("TXT", "*.txt");
        extensionFilters.put("MD", "*.md");

        /* More on System.getProperty() method: https://docs.oracle.com/javase/tutorial/essential/environment/sysprop.html */
        Utils.configureFileChooser("Choose word list", "user.home", extensionFilters);

        original.textProperty().addListener(changeTextListener(originalFile));
        translated.textProperty().addListener(changeTextListener(translatedFile));
        originalPreview.textProperty().addListener(changeTextPreviewListener(originalFile));
        translatedPreview.textProperty().addListener(changeTextPreviewListener(translatedFile));
        originalFile.textProperty().addListener(changeTextListener(originalFile, original));
        translatedFile.textProperty().addListener(changeTextListener(translatedFile, translated));

        original.setOnDragOver(dragEventHandler(original));
        original.setOnDragDropped(dropEventHandler(original));
        translated.setOnDragOver(dragEventHandler(translated));
        translated.setOnDragDropped(dropEventHandler(translated));

        wordLists.prefHeightProperty().bind(wordListsContainer.heightProperty());
        wordLists.prefWidthProperty().bind(wordListsContainer.widthProperty());
        listName.prefWidthProperty().bind(manager.widthProperty());
        originalLanguage.prefWidthProperty().bind(manager.widthProperty());
        translatedLanguage.prefWidthProperty().bind(manager.widthProperty());
        originalFile.prefWidthProperty().bind(manager.widthProperty());
        translatedFile.prefWidthProperty().bind(manager.widthProperty());

        original.setBorder(new Border(new BorderStroke(new Color(0.1961, 0.1961, 0.1961, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));
        translated.setBorder(new Border(new BorderStroke(new Color(0.1961, 0.1961, 0.1961, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));
        originalPreview.setBorder(new Border(new BorderStroke(new Color(0.1961, 0.1961, 0.1961, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));
        translatedPreview.setBorder(new Border(new BorderStroke(new Color(0.1961, 0.1961, 0.1961, 0.1), BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(1))));

        delListBtn = new JFXButton("Delete List");
        delListBtn.setButtonType(JFXButton.ButtonType.RAISED);
        delListBtn.setOnMouseClicked(e -> delList());
        delListBtn.getStyleClass().add("custom-jfx-button-raised");
        delListBtn.setDisable(true);
        manager.getChildren().add(delListBtn);

        wordLists.depthProperty().set(1);
        wordLists.setItems(wordListsDynamic);
        wordLists.setCellFactory(param -> Utils.wordListCell());

        wordLists.setOnMouseClicked(event -> wordListOnClick());

        for (WordList w : Main.wordList) {
            wordLists.getItems().add(w);
        }
    }

    private ChangeListener<String> changeTextListener(JFXTextField target) {
        return (observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) {
                target.setDisable(true);
            } else {
                target.setDisable(false);
            }
        };
    }

    private ChangeListener<String> changeTextPreviewListener(JFXTextField target) {
        return (observable, oldValue, newValue) -> {
            if (newValue.isEmpty()) {
                target.setText("");
            }
        };
    }

    private ChangeListener<String> changeTextListener(JFXTextField source, JFXTextArea target) {
        return (observable, oldValue, newValue) -> {
            if (!newValue.isEmpty()) {
                target.setDisable(true);

                if (Utils.isValidPath(newValue)) {
                    File file = new File(newValue);

                    if (file.isFile()) {
                        if (source == originalFile) {
                            originalPreview.setVisible(true);
                            originalPreview.setText(Utils.readFile(file));

                            if (originalPreview.getText().isEmpty()) {
                                errorParsing(file.getAbsolutePath());
                            }
                        } else if (source == translatedFile) {
                            translatedPreview.setVisible(true);
                            translatedPreview.setText(Utils.readFile(file));

                            if (translatedPreview.getText().isEmpty()) {
                                errorParsing(file.getAbsolutePath());
                            }
                        }
                    } else {
                        if (source == originalFile) {
                            originalPreview.setVisible(false);
                        } else if (source == translatedFile) {
                            translatedPreview.setVisible(false);
                        }
                    }
                }
            } else {
                target.setDisable(false);
                originalPreview.setVisible(false);
                translatedPreview.setVisible(false);
            }
        };
    }

    private EventHandler<DragEvent> dragEventHandler(JFXTextArea target) {
        return event -> {
            if (event.getGestureSource() != target
                    && event.getDragboard().hasFiles()) {
                /* allow for both copying and moving, whatever user chooses */
                event.acceptTransferModes(TransferMode.COPY_OR_MOVE);
            }

            event.consume();
        };
    }

    private EventHandler<DragEvent> dropEventHandler(JFXTextArea target) {
        return event -> {
            Dragboard db = event.getDragboard();
            boolean success = false;

            if (db.hasFiles()) {
                try {
                    target.setText(String.join("\n", Files.readAllLines(db.getFiles().get(0).toPath(), Charset.forName("UTF-8"))));
                } catch (IOException e) {
                    e.printStackTrace();
                }
                success = true;
            }

            /* let the source know whether the string was successfully
             * transferred and used */
            event.setDropCompleted(success);
            event.consume();
        };
    }

    /**
     * Browses a file and assign it as the original file.
     */
    @FXML
    private void browseOriginalFile() {
        if (!originalFile.isDisable()) {
            List<File> list = browse();

            if (list != null) {
                originalPreview.setVisible(true);
                originalPreview.setText(Utils.readFile(list.get(0)));
                originalFile.setText(list.get(0).getAbsolutePath());

                if (originalPreview.getText().isEmpty()) {
                    //Safe cleaning
                    original.setText("");
                    originalFile.setText("");
                    errorParsing(list.get(0).getAbsolutePath());
                }

                if (list.size() >= 2) {
                    translatedPreview.setVisible(true);
                    translatedPreview.setText(Utils.readFile(list.get(1)));
                    translatedFile.setText(list.get(1).getAbsolutePath());

                    if (originalPreview.getText().isEmpty()) {
                        //Safe cleaning
                        translated.setText("");
                        translatedFile.setText("");
                        errorParsing(list.get(1).getAbsolutePath());
                    }
                }
            }
        }
    }

    /**
     * Browses a file and assign it as the translated file.
     */
    @FXML
    private void browseTranlatedFile() {
        if (!translatedFile.isDisable()) {
            List<File> list = browse();

            if (list != null) {
                translatedFile.setText(list.get(0).getAbsolutePath());
            }
        }
    }

    /**
     * Browses a file and open a file chooser.
     *
     * @return The list of files selected in the file chooser.
     */
    private List<File> browse() {
        return Utils.fileChooser.showOpenMultipleDialog(Utils.stage);
    }

    /**
     * Handles click on the word list control.
     */
    private void wordListOnClick() {
        if (previousSelectedIndex == wordLists.getSelectionModel().getSelectedIndex()) {
            wordLists.getSelectionModel().clearSelection();
            previousSelectedIndex = -1;
            addListBtn.setText("Add Lists");
            delListBtn.setDisable(true);
            editMode = false;
            resetFields();
        } else {
            previousSelectedIndex = wordLists.getSelectionModel().getSelectedIndex();
            addListBtn.setText("Edit Lists");
            delListBtn.setDisable(false);
            editMode = true;
            displayList();
        }
    }

    /**
     * When a word list is clicked in the list view this method allows to fill all the controls with the right content.
     */
    private void displayList() {
        selectedWordList = wordLists.getSelectionModel().getSelectedItems().get(0);
        listName.setText(selectedWordList.getName());
        originalLanguage.setText(selectedWordList.getOriginalLanguage());
        translatedLanguage.setText(selectedWordList.getTranslatedLanguage());
        original.setText(selectedWordList.getOriginalToString());
        translated.setText(selectedWordList.getTranslatedToString());
    }

    /**
     * Adds a list and saves it as a json file.
     */
    @FXML
    private void addList() {
        if (!checkValidity()) {
            Utils.DIALOG_CONTROLLER.configAccept(
                    JFXDialog.DialogTransition.CENTER,
                    "Error",
                    "Ensure you either correctly filled the text areas or the given files are well formed. " +
                            "Also don't forget to enter the list name (A list name has to be unique).",
                    (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));
        } else {
            if (!editMode) {
                for (int i = 0; i < wordLists.getItems().size(); ++i) {
                    if (wordLists.getItems().get(i).getName().equals(listName.getText())) {
                        Utils.DIALOG_CONTROLLER.configAccept(
                                JFXDialog.DialogTransition.CENTER,
                                "Error",
                                "There's already a list with this name. Please provide another name.",
                                (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));
                        return;
                    }
                }
            }

            List<String> originalContent;
            List<String> translatedContent;

            if (original.isDisable()) {
                originalPreview.setText(originalPreview.getText().replaceAll(EMPTY_LINE, "\n"));
                translatedPreview.setText(translatedPreview.getText().replaceAll(EMPTY_LINE, "\n"));
                originalContent = Arrays.asList(originalPreview.getText().split("\n"));
                translatedContent = Arrays.asList(translatedPreview.getText().split("\n"));
            } else {
                original.setText(original.getText().replaceAll(EMPTY_LINE, "\n"));
                translated.setText(translated.getText().replaceAll(EMPTY_LINE, "\n"));
                originalContent = Arrays.asList(original.getText().split("\n"));
                translatedContent = Arrays.asList(translated.getText().split("\n"));
            }

            if (originalContent.size() != translatedContent.size()) {
                Utils.DIALOG_CONTROLLER.configBinaryChoice(
                        Modality.APPLICATION_MODAL,
                        "Warning",
                        "The word lists don't contain the same amount of words. Make sure Pursuing will trim the longest lists.",
                        (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));

                if (Utils.DIALOG_CONTROLLER.getResult() == DialogController.DialogResult.YES) {
                    if (originalContent.size() > translatedContent.size()) {
                        originalContent = originalContent.subList(0, originalContent.size() - (originalContent.size() - translatedContent.size()));
                    } else {
                        translatedContent = translatedContent.subList(0, translatedContent.size() - (translatedContent.size() - originalContent.size()));
                    }
                } else {
                    return;
                }
            }

            WordList wordList;
            String name = listName.getText();

            List<Word> owl = new ArrayList<>();
            List<Word> twl = new ArrayList<>();

            for (int i = 0; i < originalContent.size(); ++i) {
                Word ow = new Word(originalContent.get(i), originalLanguage.getText());
                Word tw = new Word(translatedContent.get(i), ow, translatedLanguage.getText());
                ow.setTranslation(tw);
                owl.add(ow);
                twl.add(tw);
            }

            String ol = originalLanguage.getText();
            String tl = translatedLanguage.getText();

            if (editMode) {
                if (new File(AppSettings.DIR_RESOURCES + AppSettings.DIR_WORD_LISTS, selectedWordList.getName().replaceAll(" ", "_") + ".json").delete()) {
                    selectedWordList.setName(name);
                    selectedWordList.setOriginal(owl);
                    selectedWordList.setTranslated(twl);
                    selectedWordList.setOriginalLanguage(ol);
                    selectedWordList.setTranslatedLanguage(tl);
                    saveWordList(selectedWordList);
                    wordListOnClick();
                }
            } else {
                wordList = new WordList(name, owl, twl, ol, tl);
                wordListsDynamic.add(wordList);
                Main.wordList.add(wordList);
                saveWordList(wordList);
                resetFields();
            }

        }
    }

    /**
     * Deletes a word list.
     */
    private void delList() {
        wordListOnClick();
        wordLists.getItems().remove(selectedWordList);
        Main.wordList.remove(selectedWordList);
        resetFields();

        try {
            Files.delete(new File(AppSettings.DIR_RESOURCES + AppSettings.DIR_WORD_LISTS, selectedWordList.getName().replaceAll(" ", "_") + ".json").toPath());
        } catch (IOException e) {
            Utils.DIALOG_CONTROLLER.configAccept(
                    JFXDialog.DialogTransition.CENTER,
                    "Error",
                    "The file has not been deleted.\n" + e.getMessage(),
                    (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));
        }
    }

    /**
     * Checks validity of the relevant controls before adding a new word list.
     *
     * @return true if the controls are correctly filled, false otherwise.
     */
    private boolean checkValidity() {
        return (original.isDisable() ^ originalFile.isDisable())
                && (translated.isDisable() ^ translatedFile.isDisable())
                && !listName.getText().isEmpty();
    }

    /**
     * Saves a word list as a file in a json format.
     *
     * @param wordList The word list to save.
     */
    private void saveWordList(WordList wordList) {
        JsonObject jsonWordList = new JsonObject();
        jsonWordList.addProperty("name", wordList.getName());
        jsonWordList.addProperty("original_language", wordList.getOriginalLanguage());
        jsonWordList.addProperty("translated_language", wordList.getTranslatedLanguage());

        JsonObject list = new JsonObject();
        IntStream.range(0, wordList.getOriginal().size()).forEach(i -> list.addProperty(wordList.getOriginal().get(i).getWord(), wordList.getOriginal().get(i).getTranslation().getWord()));
        jsonWordList.add("list", list);
        Utils.writeAndSaveFile(new File(AppSettings.DIR_RESOURCES + AppSettings.DIR_WORD_LISTS, wordList.getName().replaceAll(" ", "_") + ".json"), jsonWordList.toString(), false);

        /*
        {
            "name": "wordList"
            "original_language": "fr"
            "translated_language": "en"
            "list": {
                "word1": "tword1",
                "word2": "tword2"
            }
        }
         */
    }

    /**
     * Displays a dialog when an error occurs.
     *
     * @param fileName The name of the file whose the parsing failed.
     */
    private void errorParsing(String fileName) {
        Utils.DIALOG_CONTROLLER.configAccept(
                JFXDialog.DialogTransition.CENTER,
                "File parsing error",
                "An error has occurred while parsing '" + fileName + "'. This file might be corrupted.",
                (StackPane) context.getRegisteredObject(Utils.CONTENT_PANE));
    }

    /**
     * Resets all the field when adding a new list.
     */
    private void resetFields() {
        original.setText("");
        translated.setText("");
        originalPreview.setText("");
        translatedPreview.setText("");
        listName.setText("");
        originalLanguage.setText("");
        translatedLanguage.setText("");
        originalFile.setText("");
        translatedFile.setText("");
    }
}
