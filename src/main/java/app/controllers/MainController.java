package app.controllers;

import app.constants.AppSettings;
import app.utils.ExtendedAnimatedFlowContainer;
import com.jfoenix.controls.*;
import io.datafx.controller.ViewController;
import io.datafx.controller.flow.Flow;
import io.datafx.controller.flow.FlowHandler;
import io.datafx.controller.flow.context.FXMLViewFlowContext;
import io.datafx.controller.flow.context.ViewFlowContext;
import javafx.animation.Transition;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;

import javax.annotation.PostConstruct;

import static io.datafx.controller.flow.container.ContainerAnimations.SWIPE_LEFT;

@ViewController(value = "/fxml/main.fxml", title = "Word Test")
public class MainController {

    @FXMLViewFlowContext
    private ViewFlowContext context;

    @FXML
    private StackPane root;

    @FXML
    private StackPane menuBurgerContainer;
    @FXML
    private JFXHamburger menuBurger;

    @FXML
    private StackPane optionsBurger;
    @FXML
    private JFXRippler optionsRippler;
    @FXML
    private JFXDrawer drawer;

    private JFXPopup toolbarPopup;

    @PostConstruct
    public void init() throws Exception {
        // Init the menu hamburger associated animations
        drawer.setOnDrawerOpening(e -> {
            final Transition animation = menuBurger.getAnimation();
            animation.setRate(1);
            animation.play();
        });

        drawer.setOnDrawerClosing(e -> {
            final Transition animation = menuBurger.getAnimation();
            animation.setRate(-1);
            animation.play();
        });

        menuBurgerContainer.setOnMouseClicked(e -> {
            if (drawer.isClosed() || drawer.isClosing()) {
                drawer.open();
            } else {
                drawer.close();
            }
        });

        FXMLLoader loader = new FXMLLoader(MainController.class.getResource("/fxml/popup/option_popup.fxml"));
        loader.setController(new InputController());
        toolbarPopup = new JFXPopup(loader.load());

        optionsBurger.setOnMouseClicked(e -> toolbarPopup.show(optionsBurger,
                JFXPopup.PopupVPosition.TOP, JFXPopup.PopupHPosition.RIGHT, -12, 15));

        // Create the inner flow and content
        context = new ViewFlowContext();
        // Set the default controller
        Flow innerFlow = new Flow(AppSettings.HOME_CONTENT);

        final FlowHandler flowHandler = innerFlow.createHandler(context);
        context.register("ContentFlowHandler", flowHandler);
        context.register("ContentFlow", innerFlow);

        // This is where the animation is set for the AddEditWordListsController.class container.
        final Duration containerAnimationDuration = Duration.millis(320);
        drawer.setContent(flowHandler.start(new ExtendedAnimatedFlowContainer(containerAnimationDuration, SWIPE_LEFT)));
        context.register("ContentPane", drawer.getContent().get(0));

        // Side controller will add links to the content flow
        Flow sideMenuFlow = new Flow(SideMenuController.class);
        final FlowHandler sideMenuFlowHandler = sideMenuFlow.createHandler(context);
        drawer.setSidePane(sideMenuFlowHandler.start(new ExtendedAnimatedFlowContainer(containerAnimationDuration,
                SWIPE_LEFT)));

        AppSettings.currentController = AppSettings.HOME_CONTENT.getSimpleName();
    }

    private static final class InputController {

        @FXML
        private JFXListView<?> toolbarPopupList;

        // Close application
        @FXML
        private void submit() {
            if (toolbarPopupList.getSelectionModel().getSelectedIndex() == 0) {
                Platform.exit();
            }
        }
    }
}
