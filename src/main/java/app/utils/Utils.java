package app.utils;

import app.controllers.AddEditWordListsController;
import app.controllers.DialogController;
import app.models.WordList;
import com.jfoenix.controls.JFXDialog;
import com.jfoenix.controls.JFXListCell;
import javafx.fxml.FXMLLoader;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.awt.*;
import java.io.*;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.InvalidPathException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static java.nio.charset.StandardCharsets.UTF_8;

public final class Utils {

    public static Stage stage;
    public static final FileChooser fileChooser = new FileChooser();

    private static Desktop desktop = Desktop.getDesktop();

    public static final String CONTENT_PANE = "ContentPane";
    public static final DialogController DIALOG_CONTROLLER = new DialogController();
    private static JFXDialog DIALOG;

    public static JFXListCell<WordList> wordListCell() {
        return new JFXListCell<WordList>() {
            @Override
            protected void updateItem(WordList item, boolean empty) {
                super.updateItem(item, empty);

                if (empty || item == null || item.getName() == null) {
                    setText(null);
                } else {
                    setText(item.getName());
                }
            }
        };
    }

    static {
        FXMLLoader loader = new FXMLLoader(AddEditWordListsController.class.getResource("/fxml/popup/dialog.fxml"));
        loader.setController(DIALOG_CONTROLLER);

        try {
            DIALOG = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private Utils() {
    }

    public static boolean isValidPath(String path) {
        try {
            Paths.get(path);
        } catch (InvalidPathException | NullPointerException ex) {
            return false;
        }
        return true;
    }

    public static void configureFileChooser(String title, String initialDirectory, Map<String, String> extensionFilters) {
        fileChooser.setTitle(title);
        fileChooser.setInitialDirectory(new File(System.getProperty(initialDirectory)));

        List<FileChooser.ExtensionFilter> extensionFiltersList = new ArrayList<>();
        String[] keys = extensionFilters.keySet().toArray(new String[0]);

        for (int i = 0; i < extensionFilters.size(); ++i) {
            extensionFiltersList.add(new FileChooser.ExtensionFilter(keys[i], extensionFilters.get(keys[i])));
        }

        fileChooser.getExtensionFilters().addAll(extensionFiltersList);

        /*
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("All Images", "*.*"),
                new FileChooser.ExtensionFilter("JPG", "*.jpg"),
                new FileChooser.ExtensionFilter("PNG", "*.png"));
        */
    }

    public static void openFile(File file, Class klass) {
        try {
            desktop.open(file);
        } catch (IOException ex) {
            Logger.getLogger(klass.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static String readFile(File file) {
        StringBuilder content = new StringBuilder();

        try (InputStreamReader isr = new InputStreamReader(new FileInputStream(file), UTF_8)) {
            int character;
            while ((character = isr.read()) != -1) {
                content.append((char) character);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return "";
        }
        return content.toString();
    }

    public static boolean writeAndSaveFile(Path file, List<String> content) {
        try {
            Files.write(file, content, Charset.forName("UTF-8"));
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static boolean writeAndSaveFile(File file, String content, boolean append) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, append))) {
            writer.write(content);
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static boolean writeAndSaveFile(String fileName, String content, boolean append) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName, append))) {
            writer.write(content);
        } catch (IOException e) {
            return false;
        }

        return true;
    }

    public static void setStage(Stage stage) {
        Utils.stage = stage;
    }
}
