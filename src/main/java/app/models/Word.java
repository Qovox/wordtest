package app.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class Word {

    private final StringProperty word = new SimpleStringProperty(this, "Word");
    private Word translation;
    private final StringProperty language = new SimpleStringProperty(this, "Language");
    private boolean answer;

    public Word(String word) {
        this.word.setValue(word);
    }

    public Word(String word, Word translation) {
        this(word);
        this.translation = translation;
    }

    public Word(String word, String language) {
        this(word);
        this.language.setValue(language);
    }

    public Word(String word, Word translation, String language) {
        this(word);
        this.translation = translation;
        this.language.setValue(language);
    }

    public String getWord() {
        return word.get();
    }

    public StringProperty wordProperty() {
        return word;
    }

    public void setWord(String word) {
        this.word.set(word);
    }

    public Word getTranslation() {
        return translation;
    }

    public void setTranslation(Word translation) {
        this.translation = translation;
    }

    public boolean isAnswer() {
        return answer;
    }

    public void setAnswer(boolean answer) {
        this.answer = answer;
    }
}
