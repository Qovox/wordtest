package app.models;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.util.List;

public class WordList {

    private final StringProperty name = new SimpleStringProperty(this, "WordList");
    private List<Word> original;
    private List<Word> translated;
    private final StringProperty originalLanguage = new SimpleStringProperty(this, "Original Language");
    private final StringProperty translatedLanguage = new SimpleStringProperty(this, "Translated Language");

    public WordList(String name, List<Word> original, List<Word> translated, String originalLanguage, String translatedLanguage) {
        this.name.setValue(name);
        this.original = original;
        this.translated = translated;
        this.originalLanguage.setValue(originalLanguage);
        this.translatedLanguage.setValue(translatedLanguage);
    }

    public String getName() {
        return name.get();
    }

    public StringProperty nameProperty() {
        return name;
    }

    public void setName(String name) {
        this.name.setValue(name);
    }

    public String getOriginalLanguage() {
        return originalLanguage.get();
    }

    public StringProperty originalLanguageProperty() {
        return originalLanguage;
    }

    public void setOriginalLanguage(String originalLanguage) {
        this.originalLanguage.set(originalLanguage);
    }

    public String getTranslatedLanguage() {
        return translatedLanguage.get();
    }

    public StringProperty translatedLanguageProperty() {
        return translatedLanguage;
    }

    public void setTranslatedLanguage(String translatedLanguage) {
        this.translatedLanguage.set(translatedLanguage);
    }

    public List<Word> getOriginal() {
        return original;
    }

    public void setOriginal(List<Word> original) {
        this.original = original;
    }

    public List<Word> getTranslated() {
        return translated;
    }

    public void setTranslated(List<Word> translated) {
        this.translated = translated;
    }

    public String getOriginalToString() {
        return listToString(original);
    }

    public String getTranslatedToString() {
        return listToString(translated);
    }

    public String listToString(List<Word> list) {
        StringBuilder result = new StringBuilder();

        for (int i = 0; i < list.size(); ++i) {
            result.append(list.get(i).getWord()).append("\n");
        }

        return result.toString();
    }

    public String toString() {
        return name.get();
    }
}
